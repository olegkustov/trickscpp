#include <iostream>

using namespace std;

/*
    Source: https://mklimenko.github.io/english/2018/08/22/robust-endian-swap/

    The idea is simple: we define a template function, in which there is a union, 
    containing both the passed value and the std::array of the 
    corresponding size. We create two objects of such 
    union and perform the reverse copying from one to another. 
    All the extra assignments will be optimized out by the compiler, 
    which will make this code fast and efficient.
*/

template <typename T>
void SwapEndian(T &val) {
    union U {
        T val;
        std::array<std::uint8_t, sizeof(T)> raw;
    } src, dst;

    src.val = val;
    std::reverse_copy(src.raw.begin(), src.raw.end(), dst.raw.begin());
    val = dst.val;
}

/*
    There is also a SFINAE-d version, if you would like to make sure you won’t pass anything wrong:
    
    The good thing about this is that we have a template for all the corner cases and possible types. 
    But the greatest is that the compiler was able to optimize both of these functions into one assembly
    command bswap. Some instructions sets haven’t got that command and if this will become the 
    bottleneck of your program, you can make a template specialization for the case you need and have a 
    well-oiled and working common case, just like this:
*/

template <typename T>
void SwapEndian(T &val, typename std::enable_if<std::is_arithmetic<T>::value, std::nullptr_t>::type = nullptr);

template <typename T>
void SwapEndian(T &val) {
    union U {
        T val;
        std::array<std::uint8_t, sizeof(T)> raw;
    } src, dst;

    src.val = val;
    std::reverse_copy(src.raw.begin(), src.raw.end(), dst.raw.begin());
    val = dst.val;
}

template<>
void SwapEndian<std::uint32_t>(std::uint32_t &value) {
    std::uint32_t tmp = ((value << 8) & 0xFF00FF00) | ((value >> 8) & 0xFF00FF);
    value = (tmp << 16) | (tmp >> 16);
}

int main()
{


  return 0;
}